#!/bin/bash

# ------------------------------------------------------
# Install configuration scripts
# ------------------------------------------------------
mkdir /mnt/archinstall/archinstall
cp 2-configuration.sh /mnt/archinstall/archinstall/
cp 3-yay.sh /mnt/archinstall/archinstall/
cp 4-zram.sh /mnt/archinstall/archinstall/
cp 5-timeshift.sh /mnt/archinstall/archinstall/
cp 6-preload.sh /mnt/archinstall/archinstall/
cp snapshot.sh /mnt/archinstall/archinstall/

# ------------------------------------------------------
# Chroot to installed sytem
# ------------------------------------------------------
arch-chroot /mnt/archinstall ./archinstall/config.sh

